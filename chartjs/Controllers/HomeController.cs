﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace chartjs.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Data( int? type, int? company)
        {
            //這邊就從DB抓取要的資料 再包裝成要回傳的格式
            var dataset = new object[]
            { 
                new { 
                    label = "2021",
                    borderColor= "green",
                    data = new[] {
                            new { x = "一月", y = "100" },
                            new { x =  "二月", y = "120" },
                            new { x = "三月", y = "110" },
                            new { x = "四月" , y = "90" }
                            }
                },
                new {
                    label = "2022",
                    borderColor= "red",
                    data = new[] {
                            new { x = "一月", y = "90" },
                            new { x =  "二月", y = "110" },
                            new { x = "三月", y = "120" },
                            new { x = "四月" , y = "110" }
                            }
                }
            };

            return Json(dataset, JsonRequestBehavior.AllowGet);
        }
    }
}